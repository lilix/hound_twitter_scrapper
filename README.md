# HoundTwitterScrapper

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `hound_twitter_scrapper` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:hound_twitter_scrapper, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/hound_twitter_scrapper](https://hexdocs.pm/hound_twitter_scrapper).

Usage:

$~ phantomjs --webdriver 5555

$~ iex -S mix 

	iex> HoundTwitterScrapper.hello



https://github.com/HashNuke/hound

https://github.com/HashNuke/hound/blob/master/notes/configuring-hound.md

Hound documentation https://hexdocs.pm/hound
