defmodule HoundTwitterScrapper do
  use Hound.Helpers

  @url "https://twitter.com/_lil__"

  @moduledoc """
  Documentation for Scrapper.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Scrapper.hello()
      :world

  """
  def hello do
    Hound.start_session()
    navigate_to(@url)
    location_elem = find_element(:css, "div[data-testid='UserProfileHeader_Items'] .r-qvutc0")
    location = attribute_value(location_elem, "innerText")
    
    description_elem = find_element(:css, "div[data-testid='UserDescription']")

    description = attribute_value(description_elem, "innerText")

    {location, description}

    # config = %WebDriver.Config{browser: :chrome, name: :test_browser}
    #WebDriver.start_browser config
    #WebDriver.start_session :browser, :session
    #{:ok, result} = WebDriver.Session.url :session, "http://twitter.com/_lil__"
    
  end
end
